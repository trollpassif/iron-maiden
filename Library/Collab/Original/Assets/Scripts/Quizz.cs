﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quizz : MonoBehaviour
{
    public GameObject templateQuestion;
    public GameObject templateReponse;

    public string[] correctAnswers = new string[5];
    public string[] questions = new string[5];
    public string[] textesReponses1 = new string[2]; 
    public string[] textesReponses2 = new string[2];
    public string[] textesReponses3 = new string[2];
    public string[] textesReponses4 = new string[2];
    public string[] textesReponses5 = new string[2];

    private GameObject question;
    private GameObject reponse1;
    private GameObject reponse2;
    private GameObject reponse3;
    public int index;

    //private GameControl _gameControl;
    
    

    [ContextMenu("Launch Quizz")]
    public void startQuizz(){
        question = Instantiate(templateQuestion);
        reponse1 = Instantiate(templateReponse);
        reponse2 = Instantiate(templateReponse);
        reponse3 = Instantiate(templateReponse);

        question.transform.position = new Vector3(0, 4.5f, 0);
        reponse1.transform.position = new Vector3(-6, 2, 0);
        reponse2.transform.position = new Vector3(0, 2, 0);
        reponse3.transform.position = new Vector3(6, 2, 0);

        changeQuestion(index);

    }

    [ContextMenu("New Question")]
    public void changeQuestion(int index){
        switch(index){
            case 1 :
                question.GetComponentInChildren<Text>().text = questions[0];
                reponse1.GetComponentInChildren<Text>().text = correctAnswers[0];
                reponse1.tag ="CorrectAnswer";
                reponse2.GetComponentInChildren<Text>().text = textesReponses1[0];
                reponse2.tag = "Answer";
                reponse3.GetComponentInChildren<Text>().text = textesReponses1[1];
                reponse3.tag = "Answer";
                break;
            case 2 : 
                question.GetComponentInChildren<Text>().text = questions[1];
                reponse1.GetComponentInChildren<Text>().text = correctAnswers[1];
                reponse1.tag ="CorrectAnswer";
                reponse2.GetComponentInChildren<Text>().text = textesReponses2[0];
                reponse2.tag = "Answer";
                reponse3.GetComponentInChildren<Text>().text = textesReponses2[1];
                reponse3.tag = "Answer";
                break;
            case 3 :
                question.GetComponentInChildren<Text>().text = questions[2];
                reponse1.GetComponentInChildren<Text>().text = textesReponses3[0];
                reponse1.tag ="Answer";
                reponse2.GetComponentInChildren<Text>().text = correctAnswers[2];
                reponse2.tag = "CorrectAnswer";
                reponse3.GetComponentInChildren<Text>().text = textesReponses3[1];
                reponse3.tag = "Answer";
                break;
            case 4 :
                question.GetComponentInChildren<Text>().text = questions[3];
                reponse1.GetComponentInChildren<Text>().text = textesReponses4[1];
                reponse1.tag = "Answer";
                reponse2.GetComponentInChildren<Text>().text = textesReponses4[0];
                reponse2.tag = "Answer";
                reponse3.GetComponentInChildren<Text>().text = correctAnswers[3];
                reponse3.tag = "CorrectAnswer";
                break;
            case 5 :
                question.GetComponentInChildren<Text>().text = questions[4];
                reponse1.GetComponentInChildren<Text>().text = textesReponses5[1];
                reponse1.tag ="Answer";
                reponse2.GetComponentInChildren<Text>().text = textesReponses5[0];
                reponse2.tag = "Answer";
                reponse3.GetComponentInChildren<Text>().text = correctAnswers[4];
                reponse3.tag = "CorrectAnswer";
                break;
        }
    }
    [ContextMenu("End Quizz")]
    public void endQuizz(){
            Destroy(reponse1);
            Destroy(reponse2);
            Destroy(reponse3);
            Destroy(question);

            if (GameControl.Instance._score <= 4)
            {
                Bird.anim.SetBool("idle", true);
            }
            else
            {
                //le code pour quand le joueur à tout eu juste et commencer le run
            }
    }

}
