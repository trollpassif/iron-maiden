﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
    public bool IsDead = false; //Has the player collided with a wall?

    public static Animator anim;  

    private KeyCode _myJump = KeyCode.Space;
    public SpriteRenderer render;
    public BoxCollider2D bc2d;
    
    private bool _jumpEnCours = false;
    public AnimationCurve jumpCurve;
    public float timeJump;
    public float timeFactor;
    public float hauteurJump;
    [SerializeField] private Vector2 initPos;

    public static int Point;
    public Text txtVie;
  
    
    private float _vitesse;
    public dataInt _vie = new dataInt();


    private int _indexQuestion = 1;

    public Quizz quizz;
    


   
 
    void Start()
    {

        quizz.startQuizz();
        
        
        txtVie.text = _vie.Value.ToString();
        /*audioSource.clip = audioClips;
        audioSource.Play();*/
       
        
        Time.timeScale = 2;
        //Get reference to the Animator component attached to this GameObject.
        anim = GetComponent<Animator>();

    }

/* #if UNITY_EDITOR avant les debug.log pour être sûr qu'ils soient fait seulemtn dans unity*/
    void Update()
    {
        //Don't allow control if the bird has died.
        if (IsDead)
        {
            return;
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            if (Input.GetAxis("Horizontal") >= 0)
            {
                _vitesse = (Input.GetAxis("Horizontal") + 2.2f) * Time.deltaTime;
            }
            else
            {
                _vitesse = (Input.GetAxis("Horizontal") - 2.2f) * Time.deltaTime;
            }

            transform.position += new Vector3(_vitesse, 0, 0);
        }
        else
        {
            _vitesse = (Input.GetAxis("Horizontal") + 0f) * Time.deltaTime;
            transform.position += new Vector3(_vitesse, 0, 0);
        }

        if (Input.GetKeyDown(_myJump) && !_jumpEnCours)
        {
            StartCoroutine(audioComponent.Instance.SetSFX("saut"));
            initPos = transform.position;
            StopCoroutine(Jump());
            StartCoroutine(Jump());
        }
    }

    private IEnumerator Jump()

    {
            timeJump = 0;
            _jumpEnCours = true;
            while (timeJump < timeFactor)
            {
                timeJump += Time.deltaTime;


                transform.position = new Vector3(initPos.x, initPos.y, 0) + new Vector3(0, jumpCurve.Evaluate(timeJump / timeFactor) * hauteurJump, 0);
                yield return null;
            }

            transform.position = new Vector3(transform.position.x, initPos.y, 0);
            _jumpEnCours = false;
        
    }

   



    private void OnTriggerEnter2D(Collider2D other)
    {
        var tagCol = other.tag;
              
            
       
            if(tagCol.Substring(tagCol.Length - 4) == "swer")
            {
                other.enabled = false;
                StopCoroutine(toucheCaseReponse(other));
                StartCoroutine(toucheCaseReponse(other));
            }
            else if (other.CompareTag("Obstacle"))
            {
                GetHit();
                StopAllCoroutines();
                StartCoroutine(Flash());
                StartCoroutine(Invincible());
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Bonus"))
            {
                Destroy(other.gameObject);
            }
        

    }
    
    void GetHit()
    {
        _vie.Value -= 1;
    }

  

   IEnumerator Flash()
    {
        
        for (int i = 0; i <= 6; i++) {
            render.enabled = false;
            yield return new WaitForSeconds(0.2f);
            render.enabled = true;
            yield return new WaitForSeconds(0.2f);
            i++;
        }
    }

   IEnumerator Invincible()
   {
       bc2d.enabled = false;
       yield return new WaitForSecondsRealtime(1);
       bc2d.enabled = true;
       
   }

   IEnumerator toucheCaseReponse(Collider2D tag)
   {
       
       if (tag.CompareTag("CorrectAnswer"))
       {
           GameControl.Instance._score += 1;
       }

       _indexQuestion += 1;
       if (_indexQuestion <= 5)
       {
           yield return new WaitForSeconds(1);
           quizz.changeQuestion(_indexQuestion);
           tag.enabled = true;
       }
       else
       {
           quizz.endQuizz();
       }

   }

 


 
}