﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objDestroyer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Obstacle") && (other.transform.position.x<=0))
        {
            Destroy(other.gameObject);
        }

    }
}
