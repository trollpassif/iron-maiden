﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour 
{
	public static GameControl Instance;			//A reference to our game control script so we can access it statically.
	public Text scoreText;							//A reference to the object that displays the text which appears when the player dies.

	public int _score = 0;						//The player's score.
	public bool gameOver = false;
	public changeBiome changeBiome;
	public bool quizzOver = false;
	private float timer = 0;
	private bool canChange = false;
	private int timerInt = 0; 
	
	

	void Awake()
	{
		//If we don't currently have a game control...
		if (Instance == null)
			//...set this one to be it...
			Instance = this;
		//...otherwise...
		else if(Instance != this)
			//...destroy this one because it is a duplicate.
			Destroy (gameObject);
		
		changeBiome.spawnDecor(0);
	}

	void FixedUpdate()
	{
		//If the game is not over, increase the score...
		timerInt = Mathf.RoundToInt(timer);
		if(quizzOver) {
			_score++;
			timer += Time.deltaTime;
		}

		if(timerInt >= 0){
			if (canChange){
				changeBiome.biomeChange(0);
			}
		}
        if(timerInt >=  95){
			canChange = !canChange;
			if(canChange){
				changeBiome.biomeChange(1);
				canChange = !canChange;			}
		}
		if(timerInt	 >= 155){
			canChange = !canChange;
			if(canChange){
				changeBiome.biomeChange(2);
			}
		}


		//...and adjust the score text.
		scoreText.text = "Score: " + _score.ToString();
	}
	
}