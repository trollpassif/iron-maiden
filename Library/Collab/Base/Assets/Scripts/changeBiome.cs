﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBiome : MonoBehaviour
{

    public Sprite[] imagesBG = new Sprite[3];
    public GameObject background;
    public int indexBG;
    private GameObject bg;   

    void spawnBG(int index){
        bg = Instantiate(background);
        bg.GetComponent<SpriteRenderer>().sprite = imagesBG[index];
    }
    void changeBiome(){
        Destroy(this);
        spawnBG(indexBG);
    }
}
