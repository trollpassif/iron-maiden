﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class dataInt 
{
    [SerializeField] 
    private int value;
    public IntChange myEventInt;


    public int Value
    {
        get => value;
        set
        {
            this.value = value;
            myEventInt.Invoke(value);
        }
    }
}


[Serializable]
public class IntChange : UnityEvent<int>
{


}