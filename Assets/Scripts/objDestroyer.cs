﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objDestroyer : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Obstacle") || (other.CompareTag("Bonus")))
        {
            Destroy(other.gameObject);
        }

    }
}
