﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scorm;
using UnityEngine;
using UnityEngine.UI;

public class ScormAPI : MonoBehaviour
{
    static IScormService _scormService;

    private new static string _name;
    [SerializeField] private Text NomJoueur;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(2.0f);
        #if UNITY_EDITOR
                _scormService = new ScormPlayerPrefsService (); 
        #else
                _scormService = new ScormService (); // Real implementation
        #endif
        
        
        bool result = _scormService. Initialize (Scorm.Version.Scorm_2004);
        
        
        if (result)
        {
            Bird.nomJoueur = _scormService.GetLearnerName();
            _name = Bird.nomJoueur;
            if (!(_name.Equals("")))
            {
                NomJoueur.text =_name;
            }

            _scormService.SetMinScore(0); // Sets a min score of 0
            _scormService.SetRawScore(0); // Sets score at 0
        }

    }

   

    public static void EndGameScorm(float score)
    {
        _scormService.SetRawScore(score);
        _scormService.Commit();
        _scormService.Finish();
    }
}
