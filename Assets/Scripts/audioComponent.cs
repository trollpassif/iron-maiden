﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class audioComponent : MonoBehaviour
{

    public static audioComponent Instance;

    public AudioSource audioMusique;
    public AudioClip clipSaut;
    public AudioClip clipMort;
    public AudioClip clipBonus;
    public AudioClip musicJeu;
    void Awake()
    {

        
        //If we don't currently have a game control...
        if (Instance == null)
            //...set this one to be it...
            Instance = this;
        //...otherwise...
        else if(Instance != this)
            //...destroy this one because it is a duplicate.
            Destroy (gameObject);
    }
    
    public IEnumerator SetSFX(String  clipSFX)
    {
        AudioSource audio = GetComponent<AudioSource>();
        switch (clipSFX)
        {
            case "saut" : 
                audio.clip =  clipSaut;
                break;
            case "mort" : 
                audio.clip =  clipMort;
                break;
            case "bonus" : 
                audio.clip =  clipBonus;
                break;
        }
        audio.Play();
        yield return new WaitForSeconds(audio.clip.length);
    }
    
    public IEnumerator FadeOut(float FadeTime) {
        AudioSource audio =  keepItrolling.Instance.gameObject.GetComponent<AudioSource>();
        
        float startVolume = audio.volume;
        while (audio.volume > 0) {
            audio.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }
        audio.Stop();
    }

    public IEnumerator FadeIn(float FadeTime) {
      
        audioMusique.clip = musicJeu;
        audioMusique.Play();
        audioMusique.volume = 0f;
        while (audioMusique.volume < 0.17f) {
            audioMusique.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
    }
}
