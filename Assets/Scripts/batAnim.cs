﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class batAnim : MonoBehaviour
{
    public AnimationCurve batAnimCurve;
    public float timebatAnim;
    public float timeFactor;
    public float hauteurbatAnim;
    [SerializeField] private Vector2 initPos;
    // Start is called before the first frame update
    void Start()
    {
        initPos = transform.position;
        timebatAnim = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timebatAnim += Time.deltaTime;
        transform.position = new Vector3(initPos.x, initPos.y, 0) + new Vector3(0, batAnimCurve.Evaluate(timebatAnim / timeFactor), 0);
    }
}