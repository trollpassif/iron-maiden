﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScoreBoard : MonoBehaviour
{
    public ScoreContent content;

  

    IEnumerator GetRequest(string uri, bool generateJson)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
// Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                var t = webRequest.downloadHandler.text;
                if (generateJson)
                    try
                    {
                        content = JsonUtility.FromJson<ScoreContent>(t);
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }

                Debug.Log(pages[page] + ":\nReceived: " + t);
            }
        }

       
    }

    public void SendScore(string name, int score, int time)
    {
        StartCoroutine(GetRequest(
            $"http://dreamlo.com/lb/6srRyb4rAUCunycF2hKlkQwmg39v_Fj0qsEzkM9u4_Bw/add/{name}/{score}/{time}",false));
    }

   
}