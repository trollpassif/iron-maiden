﻿using System;
[Serializable]
public class Entry
{
    public string name;
    public string score;
    public string seconds;
    public string text;
    public string date;
}
[Serializable]
public class Leaderboard
{
    public Entry[] entry;
}

[Serializable]
public class Dreamlo
{
    public Leaderboard leaderboard;
}


[Serializable]
public class ScoreContent
{
    public Dreamlo dreamlo;
}

