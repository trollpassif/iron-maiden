﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class timer : MonoBehaviour {
    Image fillImg;
    public float timeAmt;
    public float time;
    public Text timeText;
    

    public float scale=0.5f;

    // Use this for initialization
    void Start () {
        fillImg = this.GetComponent<Image>();
        time = timeAmt;
        StartCoroutine(jacques());
    }
    
    // Update is called once per frame
    /*void Update () {
        if(time  > 0){
            time -= Time.deltaTime;
            fillImg.fillAmount -= 1.0f / timeAmt * Time.deltaTime;
            timeText.text = timeAmt.ToString("F");
        }
    }*/

    private IEnumerator jacques() {
        while(time > 0){
            time = time- scale;
            fillImg.fillAmount =time / timeAmt ;
            timeText.text = time.ToString("f");
            yield return new WaitForSeconds(scale);
        }
    }
}