﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keepItrolling : MonoBehaviour
{
    // Start is called before the first frame update
    private static keepItrolling instance = null;
    public static keepItrolling Instance {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
       
    }
}
