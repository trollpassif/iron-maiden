﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour 
{
	public static GameControl Instance;			//A reference to our game control script so we can access it statically.
	public Text scoreText, finalScore;	//A reference to the object that displays the text which appears when the player dies.

	public float _score = 0f;						//The player's score.
	public bool gameOver = false;
	public changeBiome changeBiome;
	public bool quizzOver = false;
	public float timer = 0;
	private bool canChange = false;
	public int timerInt = 0; 
	
	

	void Awake()
	{
		//If we don't currently have a game control...
		if (Instance == null)
			//...set this one to be it...
			Instance = this;
		//...otherwise...
		else if(Instance != this)
			//...destroy this one because it is a duplicate.
			Destroy (gameObject);
		
		changeBiome.spawnDecor(0);
		StartCoroutine(timerChgmntBiom(1));
	}

	void FixedUpdate()
	{
		//If the game is not over, increase the score...
		timerInt = Mathf.RoundToInt(timer);
		if(quizzOver) {
			_score++;
			timer += Time.deltaTime;
		}

		


		//...and adjust the score text.
		finalScore.text = "Score: " + _score.ToString();
		scoreText.text = "Score: " + _score.ToString();
	}

	private IEnumerator timerChgmntBiom(int index)
	{
		yield return new WaitForSecondsRealtime(95);
		changeBiome.biomeChange(1);
		yield return new WaitForSecondsRealtime(60);
		changeBiome.biomeChange(2);

	}
	
}