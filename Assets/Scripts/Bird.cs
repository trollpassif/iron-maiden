﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
    public bool IsDead = false; //Has the player collided with a wall?

    public static Animator anim;  

    private KeyCode _myJump = KeyCode.Space;
    public SpriteRenderer render;
    public BoxCollider2D bc2d;
    
    private bool _jumpEnCours = false;
    public AnimationCurve jumpCurve;
    [SerializeField] private Vector2 initPos;

    public Text txtVie;
  
    
    private float _vitesse;
    public dataInt _vie = new dataInt();


    private int _indexQuestion = 1;

    public Quizz quizz;
    public ObjectSpawner spawn;
    
    private IEnumerator activeJumpCoroutine;
    public float JumpProgress { get; private set; }

    public timer tempsQuizz;
    public static String nomJoueur;
    public ScoreBoard leaderBoard;


   
 
    void Start()
    {

        quizz.startQuizz();

        _vie.Value = 5;
        txtVie.text = _vie.Value.ToString();
       
       
        
        Time.timeScale = 2;
        //Get reference to the Animator component attached to this GameObject.
        anim = GetComponent<Animator>();
        

    }

/* #if UNITY_EDITOR avant les debug.log pour être sûr qu'ils soient fait seulemtn dans unity*/
    void Update()
    {
        
        //Don't allow control if the bird has died.
        if (IsDead)
        {
            
            var scoreFin = Mathf.RoundToInt(GameControl.Instance._score);
            leaderBoard.SendScore(Bird.nomJoueur, scoreFin, GameControl.Instance.timerInt);
            return;
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            if (Input.GetAxis("Horizontal") >= 0)
            {
                _vitesse = (Input.GetAxis("Horizontal") + 2.2f) * Time.deltaTime;
            }
            else
            {
                _vitesse = (Input.GetAxis("Horizontal") - 2.2f) * Time.deltaTime;
            }

            transform.position += new Vector3(_vitesse, 0, 0);
        }
        else
        {
            _vitesse = (Input.GetAxis("Horizontal") + 0f) * Time.deltaTime;
            transform.position += new Vector3(_vitesse, 0, 0);
        }

        if (Input.GetKeyDown(_myJump) && !_jumpEnCours)
        {
            _jumpEnCours = true;
            initPos = transform.position;
            if (Input.GetKey(KeyCode.D))
            {
                if((initPos.x + 3f) <= 7f)
                {
                    initPos.x = initPos.x + 3f;
                }
                else
                {
                    initPos.x = 7f;
                }
                var finPo = new Vector3(initPos.x, initPos.y, 0);
                Jump((new Vector3(finPo.x, finPo.y, 0)), 3f, 1f);
            } else if (Input.GetKey(KeyCode.Q))
            {
                if((initPos.x - 3f) >= -7f)
                {
                    initPos.x = initPos.x - 3f;
                }
                else
                {
                    initPos.x = -7f;
                }
                
                var finPo = new Vector3(initPos.x, initPos.y, 0);
                Jump((new Vector3(finPo.x, finPo.y, 0)), 3f, 1f);
            }
            else
            {
               Jump((new Vector3(initPos.x, initPos.y, 0)), 3f, 1f);
            }
        }
    }

    public IEnumerator Spawning()
    {
        while (!IsDead)
        {
            float score = GameControl.Instance._score/1000f;
            float temps = 0.00001543f * ((GameControl.Instance.timer) * (GameControl.Instance.timer)) - 0.01944f * (GameControl.Instance.timer) + 6f;
            
            float poussee = -(-0.00001852f * ((score) * (score)) + 0.01778f * (score) + 1.0f);
            Debug.Log(poussee);
            spawn.spawnObject(poussee);
            yield return new WaitForSeconds(temps);
        }
    }
  

   



    private void OnTriggerEnter2D(Collider2D other)
    {
        var tagCol = other.tag;
              
            
       
            if(tagCol.Substring(tagCol.Length - 4) == "swer")
            {
                other.enabled = false;
                StopCoroutine(toucheCaseReponse(other));
                StartCoroutine(toucheCaseReponse(other));
            }
            else if (other.CompareTag("Obstacle"))
            {
                StartCoroutine(audioComponent.Instance.SetSFX("saut"));
                GetHit();
                StopCoroutine(Flash());
                StopCoroutine(Invincible());
                StartCoroutine(Flash());
                StartCoroutine(Invincible());
               // Destroy(other.gameObject);
            }
            else if (other.CompareTag("Bonus"))
            {
                StartCoroutine(audioComponent.Instance.SetSFX("bonus"));
                Destroy(other.gameObject);
                GameControl.Instance._score += 1000;
            }
            else if (other.CompareTag("Vie"))
            {
                StartCoroutine(audioComponent.Instance.SetSFX("bonus"));
                Destroy(other.gameObject);
                _vie.Value += 1;
            }
        

    }
    
    void GetHit()
    {
        _vie.Value -= 1;
    }

  

   IEnumerator Flash()
    {
        
        for (int i = 0; i <= 12; i++) {
            render.enabled = false;
            yield return new WaitForSeconds(0.2f);
            render.enabled = true;
            yield return new WaitForSeconds(0.2f);
            i++;
        }
    }

   IEnumerator Invincible()
   {
       bc2d.enabled = false;
       yield return new WaitForSecondsRealtime(2);
       bc2d.enabled = true;
       
   }

   IEnumerator toucheCaseReponse(Collider2D tag)
   {
       tempsQuizz.time = 10;
       
       if (tag.CompareTag("CorrectAnswer"))
       {
           GameControl.Instance._score += 1;
       }

       _indexQuestion += 1;
       if (_indexQuestion <= 5)
       {
           yield return new WaitForSeconds(1);
           quizz.changeQuestion(_indexQuestion);
           tag.enabled = true;
       }
       else
       {
           quizz.endQuizz();
           tempsQuizz.GetComponentInParent<Canvas>().enabled = false;
       }

   }

   private void Jump(Vector3 destination, float maxHeight, float time) {
            if (activeJumpCoroutine != null) {
                StopCoroutine(activeJumpCoroutine);
                activeJumpCoroutine = null;
                JumpProgress = 0.0f;
            }
            activeJumpCoroutine = JumpCoroutine(destination, maxHeight, time);
            StartCoroutine(activeJumpCoroutine);
        }
        
   private IEnumerator JumpCoroutine(Vector3 destination, float maxHeight, float time) {
            var startPos = transform.position;
            while (JumpProgress <= 1.0) {
                JumpProgress += Time.deltaTime / time;
                var height = Mathf.Sin(Mathf.PI * JumpProgress) * maxHeight;
                if (height < 0f) {
                    height = 0f;
                }
                transform.position = Vector3.Lerp(startPos, destination, JumpProgress) + Vector3.up * height;
                yield return null;
            }
            transform.position = destination;
            _jumpEnCours = false;
        }

 


 
}