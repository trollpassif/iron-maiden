﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class animScore : MonoBehaviour
{
 
    // Update is called once per frame
    void Update()
    {
        StartCoroutine(TopScore());
    }

    IEnumerator TopScore()
    {
        if (!GameControl.Instance.gameOver)
        {
            StopCoroutine(TopScore());
        }
        else
        {
            GetComponent<Animator>().SetBool("go", true);
            yield return new WaitForSeconds(5); 
            SceneManager.LoadScene(3);
        }
    }
}


