﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBiome : MonoBehaviour
{

    public Sprite[] imagesBG = new Sprite[3];
    public Sprite[] imagesSol = new Sprite[3];
    public Sprite[] imagesMidGround = new Sprite[3];

    public GameObject decor;

    // public int index;

    private GameObject bg;
    private GameObject mground; 
    private GameObject ground; 
    private GameObject ground2;
    

    private GameObject mground2; 
    private GameObject mground3;

    private Vector3 posBG = new Vector3(2f,7.1f,0);
    private Vector3 posMid = new Vector3(3.2f,0.8f,0);
    private Vector3 posSol = new Vector3(0.0f,-1.7f,0);


    [ContextMenu("Spawn new biome")]
    public void spawnDecor(int index){
        bg = Instantiate(decor);
        mground = Instantiate(decor);
        mground2 = Instantiate(decor);
        //mground3 = Instantiate(decor);
        ground = Instantiate(decor);
        ground2 = Instantiate(decor);

        // bg2 = Instantiate(decor);

        //on configure le background
        bg.GetComponent<SpriteRenderer>().sprite = imagesBG[index];
        bg.GetComponent<SpriteRenderer>().sortingOrder = -2;
        bg.transform.localPosition = posBG;
        bg.transform.localScale = new Vector3(10f,7.1f,0.0f);
        bg.GetComponent<ScrollingObject2>().scrollSpeed = -0.5f;

        //on configure le middle ground
        mground.GetComponent<SpriteRenderer>().sprite = imagesMidGround[index];
        mground.GetComponent<SpriteRenderer>().sortingOrder = -1;
        mground.transform.localPosition = posMid;
        mground.transform.localScale = new Vector3(3.2f,2.1f,0.0f);
        mground.GetComponent<ScrollingObject2>().scrollSpeed = -0.8f;

        mground2.GetComponent<SpriteRenderer>().sprite = imagesMidGround[index+3];
        mground2.transform.localPosition = posMid + new Vector3(-20f, 0.0f, 0.0f);
        mground2.GetComponent<SpriteRenderer>().sortingOrder = -1;
        // mground2.GetComponent<SpriteRenderer>().flipX = true;
        mground2.transform.localScale = new Vector3(3.2f,2.1f,0.0f);
        mground2.GetComponent<ScrollingObject2>().scrollSpeed = -0.8f;
        
        /*mground3.GetComponent<SpriteRenderer>().sprite = imagesMidGround[index];
        mground3.transform.position = posMid + new Vector3(-20.6f, 0.0f, 0.0f) + new Vector3(-20.6f, 0.0f, 0.0f);
        mground3.GetComponent<SpriteRenderer>().flipX = true;
        mground3.transform.localScale = new Vector3(3.2f,2.1f,0.0f);
        mground3.GetComponent<ScrollingObject2>().scrollSpeed = -0.8f;*/

        //on configure le sol
        ground.GetComponent<SpriteRenderer>().sprite = imagesSol[index];
        ground.transform.localPosition = posSol;
        ground.GetComponent<SpriteRenderer>().sortingOrder = 1;
        ground.transform.localScale = new Vector3(1,1f,0.0f);
        ground.GetComponent<ScrollingObject2>().scrollSpeed = -1.5f;
        
        ground2.GetComponent<SpriteRenderer>().sprite = imagesSol[index];
        ground2.transform.localPosition = posSol + new Vector3(-20f, 0.0f, 0.0f);;
        ground2.GetComponent<SpriteRenderer>().sortingOrder = 1;
        //ground2.GetComponent<SpriteRenderer>().flipX = true;
        ground2.transform.localScale = new Vector3(1,1f,0.0f);
        ground2.GetComponent<ScrollingObject2>().scrollSpeed = -1.5f;


    }

    [ContextMenu("Change biome")]
    public void biomeChange(int index){
        Destroy(bg);
        Destroy(mground);
        Destroy(mground2);
        Destroy(ground);
        Destroy(ground2);
        spawnDecor(index);
    }
}
