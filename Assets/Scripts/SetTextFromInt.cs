﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class SetTextFromInt : MonoBehaviour
{
    public Text myText;
    public float timeFactor;
    private Vector2 initPos;
    public AnimationCurve deathCurve;
    public Bird joueur;
    
    
    public void IntChange(int value)
    {
        if (value > 0)
        {
            myText.text = value.ToString();
        }else if (value == 0)
        {
            Bird.anim.SetBool("mort", true);
            initPos = joueur.transform.position;
            myText.text = value.ToString();
            joueur.IsDead = true;
            GameControl.Instance.gameOver = true;
            ScormAPI.EndGameScorm(GameControl.Instance._score);
            
            StartCoroutine(audioComponent.Instance.SetSFX("mort"));
            StartCoroutine(Death());
           
            GameControl.Instance.gameObject.SetActive(false);
        }
       
    }
    
    public IEnumerator Death()

    {
        audioComponent.Instance.FadeOut(3);
        var timeJump = 0.0f;
        while (timeJump < timeFactor)
        {
            timeJump += Time.deltaTime;


            joueur.transform.position = new Vector3(0, initPos.y, 0) + new Vector3(initPos.x,
                                     deathCurve.Evaluate(timeJump / timeFactor) * 4, 0);
            yield return null;
        }
        
        Bird.anim.SetBool("score", true);
    }
}
