﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class fadeOutStateMachine : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        FindObjectOfType<dontDestroyPls>().gameObject.SetActive(true);
        animator.SetInteger("fading", 3);
        FindObjectOfType<dontDestroyPls>().GetComponent<Canvas>().sortingOrder = 2;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (SceneManager.GetActiveScene().name == "menuPrincipal" && LoadSceneOnClick.isCredit)
        {
            SceneManager.LoadScene(2);
            FindObjectOfType<sceneFader>().GetComponent<Animator>().SetInteger("fading", 2);
        } else if (SceneManager.GetActiveScene().name == "menuPrincipal")
        {
            SceneManager.LoadScene(1);
            FindObjectOfType<sceneFader>().GetComponent<Animator>().SetInteger("fading", 2);
        }else if (SceneManager.GetActiveScene().name == "menuCredits")
        {
            SceneManager.LoadScene(0);
            FindObjectOfType<sceneFader>().GetComponent<Animator>().SetInteger("fading", 2);
        }

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
