﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject2 : MonoBehaviour 
{
	private Rigidbody2D _rb2D;
	public float scrollSpeed = -0.5f;
	

	// Use this for initialization
	void Start () 
	{
		//Get and store a reference to the Rigidbody2D attached to this GameObject.
		_rb2D = GetComponent<Rigidbody2D>();

		//Start the object moving.

		_rb2D.velocity = new Vector2(scrollSpeed, 0);
    }

	void Update()
	{
		// If the game is over, stop scrolling.
		if((GameControl.Instance.gameOver == true))
		{
			_rb2D.velocity = Vector2.zero;
		}
	}
}
