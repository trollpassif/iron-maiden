﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class ObjectSpawner : MonoBehaviour
{
    
    public Sprite[] spritesObstaclesDesert = new Sprite[3];
    public Sprite[] spritesObstaclesForest = new Sprite[3];
    public Sprite[] spritesObstaclesCity = new Sprite[3];
    public Sprite[] spriteBonus = new Sprite[2];

    public int scoreBonusVie, scoreBonusPoint;

    public int index; //0 = desert || 1 = graveyard || 2 = city 
    // public Sprite[] imagesBG = new Sprite[3];

    public GameObject objet;
 

    private int randomInt;
    private GameObject objetMouvant;
    private SpriteRenderer spriteObjet;

    // private ObjectSpawn configObs;
    // private GameObject bg;


    //a appeler quand on veut créer un objet (bonus et obstacles)
   // [ContextMenu("Spawn an object")]
    public void spawnObject(float poussee){

        objetMouvant = Instantiate(objet);
        
        spriteObjet = objetMouvant.GetComponent<SpriteRenderer>();
        if (Random.Range(1, 100) >= 25)
        {
            objetMouvant.transform.position = new Vector3(12, -1, 0);
        }
        else
        {
            objetMouvant.transform.position = new Vector3(12, 3.5f, 0);
        }

        var rb2D = objetMouvant.GetComponent<Rigidbody2D>();
        
        rb2D.velocity = new Vector2(poussee, 0);
       // configObs = objetMouvant.GetComponent<ObjectSpawn>();
       
        randomInt = Random.Range(1,100);
        //Debug.Log(randomInt);
        if(randomInt<=25){
            randomInt = Random.Range(1,100);
           // Debug.Log(randomInt);
            if(randomInt <= 40){
                objetMouvant.tag = "Vie";
                spriteObjet.sprite = spriteBonus[0];
                /*configObs.score = scoreBonusVie;
                Debug.Log(configObs);*/
            }

            objetMouvant.tag = "Bonus";
            spriteObjet.sprite = spriteBonus[1];
           /* configObs.score = scoreBonusPoint;
            Debug.Log(configObs);*/
        }
        else{
            objetMouvant.tag = "Obstacle";
            switch(index){
                case 0 :
                    spriteObjet.sprite = spritesObstaclesForest[Random.Range(0, spritesObstaclesForest.Length)];
                    break;
                case 1 :
                    spriteObjet.sprite = spritesObstaclesDesert[Random.Range(0, spritesObstaclesDesert.Length)];
                    break;
                case 2 : 
                    spriteObjet.sprite = spritesObstaclesCity[Random.Range(0, spritesObstaclesCity.Length)];
                    break;

            }
//            spriteObjet.sprite = spritesObstacles[Random.Range(0, spritesObstacles.Length)];
           // Debug.Log(configObs);
        }

    }



}
