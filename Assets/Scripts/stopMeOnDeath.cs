﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class stopMeOnDeath : MonoBehaviour
{
    // Start is called before the first frame update
    public void Update()
    {
        if (GameControl.Instance.gameOver)
        {
            GetComponent<Rigidbody2D>().simulated = false;
        }
    }
}
